# UML Anwendungsfalldiagramm (_Use Case Diagram_)

<span class="hidden-text">
https://oer-informatik.de/uml-usecase
</span>


> **tl/dr;** _(ca. 7 min Lesezeit): Welche Notationmittel bietet das UML-Anwendungsfalldiagramm? Welche davon sind in der Praxis relevant? Für welche Art Beziehung setze ich sie ein? Informationen zur Erstellung von UML-Anwendungsfalldiagrammen mit PlantUML finden sich [in einem gesonderten Use-Case-PlantUML-Artikel](https://oer-informatik.de/uml-usecase-plantuml)._

Ein Anwendungsfalldiagramm beschreibt _wie_ ein (Software)-System mit Anwenderinnen ^[Grundsätzlich: es wird die feminine gramatikalische Form gewählt, maskuline Akteure sind immer [https://twitter.com/hashtag/mitgemeint?lang=de](#mitgemeint)] interagiert. Es beschreibt, **welche Anwendungsfälle** ein System anbietet. Die Reihenfolgen oder Abläufe der Anwendungsfälle müssen jedoch auf andere Art modelliert werden.

![UML Use-Case-Diagramm](plantuml/11_Beispiel.png)

Anwendungsfalldiagramme helfen v.a. dabei, die Vollständigkeit und Korrektheit des Systemverständnisses der Projektbeteiligten (Kunden, Fachdomäne, Entwicklerinnen) abzugleichen.

## Akteurinnen und Anwendungsfälle ##


![Einfaches Anwendunsfalldiagramm mit Akteurin "Kundin", dem Anwendungsfall "Ware bestellen" und der Verbindenden Assiziation](plantuml/01_Bestandteile_UseCase.png)

**Akteurinnen** sind Menschen oder andere Systeme, die Anwendungsfälle des Systems nutzen. Sie können auch Rollen (Kundin) oder Typen zusammenfassen. Sie werden in der Regel als Strichmensch (_stick man_ ) notiert.

Mit einem **Anwendungsfall (Use Case)** erzeugt das modellierte System erkennbaren Nutzen für die zugeordneten Akteurinnen. Ein Anwendungsfall (im Beispiel oben: "Ware bestellen") fasst Aktionen zusammen, die (funktionale) Anforderungen erfüllen. Anwendungsfälle werden in der Regel als Ellipse notiert.

**Assoziationen** verbinden Anwendungsfälle mit den auslösenden oder benötigten Akteurinnen. Sie werden mit durchgezogenen Linien notiert.

## Systemgrenzen und Systemname

![UseCaseDiagram für ein Shopsystem](plantuml/02_Systemgrenzen.png)

Die **Systemgrenze** legt fest, welche Anwendungsfälle im modellierten System enthalten sind. Das abgegrenzte System trägt einen Namen und spannt einen Namensraum auf.

Alle **Akteurinnen** stehen außerhalb der Systemgrenzen - andernfalls wären sie als Teil des Systems nicht gesondert zu modellieren.

Systemgrenzen müssen nicht zwingend angegeben werden, dienen aber dem Verständnis und der Abgrenzung von Akteurinnen, Anwendungsfällen und externen Systemen.

## Vererbung zur Modellierung von Rollen der Akteurinnen

Mit Hilfe von Vererbungsbeziehungen können Akteurinnen spezialisiert werden: Im Beispiel unten ist die Prokuristin eine spezielle Mitarbeiterin, der zusätzlich zu allen Anwendungsfällen einer Mitarbeiterin auch noch über eigene Anwendungsfälle verfügt. Vererbung wird - wie in der UML üblich - mit einer geschlossenen Pfeilspitze symbolisiert:

![Vererbung von Akteurinnen im Anwendungsfalldiagramm](plantuml/03_VererbungVonAkteuren.png)


Vererbungsbeziehungen werden auch genutzt, um ODER-Beziehungen zu modellieren: Eine Geschäftsführerin oder eine Prokuristin darf Verträge unterschreiben. Solche Zusammenhänge lassen sich nur über eine generalisierte Akteurin (hier: Befugte) realisieren.

![Vererbung von Akteurinnen im Anwendungsfalldiagramm](plantuml/03_Vererbung_OderBeziehungenVonAkteuren.png)

Akteurinnen, die zwar als Generalisierung anderer Rollen modelliert werden, die aber konkret (als Instanz im OOP-Sinne) nie existieren, können als abstrakte Akteurinnen modelliert werden. Ihr Name wird kursiv geschrieben und/oder mit dem _Constraint_ \{abstract\} gekennzeichnet:

![Abstrakte Akteurinnen](plantuml/03_Vererbung_Abstrakt.png)

Hier wurde eine Akteurin oberhalb und eine unterhalb positioniert, die Vererbungshierarchie wird durch die Pfeilrichtung vorgegeben.

## Anwendungsfälle, die weitere Anwendungsfälle immer beinhalten
Sofern zur Erfüllung eines Anwendungsfalls in jedem Fall auf die Funktionalität eines zweiten Anwendungsfalls zurückgegriffen werden muss, kann dieser über eine _include_ -Beziehung verknüpft werden. Wichtig ist, dass der eingebundene Anwendungsfall auch isoliert einen abgeschlossenen Nutzen generiert (also nicht fester Bestandteil des anderen Anwendungsfalls ist). Diese "beinhaltet"-Beziehung wird durch eine gestrichelte Linie mit Pfeilspitze dargestellt, die in Richtung des einbezogenen Anwendungsfalls zeigt und die mit dem Stereotyp &laquo;include&raquo; versehen wird. Der Pfeil kann als "beinhaltet" in Pfeilrichtung gelesen werden.


![Der Usecase "Vertrag ausfertigen" beinhaltet den Usecase "Vertrag ausdrucken"](plantuml/05_include.png)

Die Gefahr ist groß über _include_ -Beziehungen Programmabläufe und Unterfunktionsaufrufe zu modellieren. Daher ist es wichtig, immer genau zu prüfen: Stellt der inkludierte Anwendungsfall wirklich einen eigenständig auslösbaren Anwendungsfall dar?


## Anwendungsfälle, die unter Umständen durch weitere Anwendungsfälle erweitert werden

Sofern ein Anwendungsfall nur unter bestimmten Umständen um die Funktionalitäten eines zweiten Anwendungsfalls erweitert wird, werden beide über eine _extend_ -Beziehung verknüpft. Zu jeder _extend_ -Beziehung _sollte_ angegeben werden, unter welcher Bedingung (_condition_ ) welcher Anwendungsfall erweitert wird.
Ein gestrichelter Pfeil zeigt vom erweiternden auf den zu erweiternden Anwendungsfall und ist mit dem Stereotyp &laquo;extend&raquo; versehen. An dieser Linie sollte eine Notiz mit _condition_ und _extension point_ notiert werden. Der  _extension point_ wird auch am Ursprungs-Anwendungsfall notiert. Der Pfeil kann als "erweitert" in Pfeilrichtung gelesen werden.

![Der Anwendungsfall "Neukunden Registrierung" erweitert den Anwendungfall "bestellen", falle es sich um einen Neukunden handelt](plantuml/06_extend.png)

## Vererbung von Anwendungsfällen

Analog zu Akteurinnen können auch Anwendungsfälle spezialisiert werden. Beispielsweise kann ein generalisierter Anwendungsfall "Artikel kaufen" bestehen aus dem Szenario:

```
1. Artikel in Warenkorb legen
2. Warenkorb bestellen
3. Kauf abwickeln
```

Die spezialisierten Anwendungsfälle ändern Details in den Szenarien, z.B. bei "Buch per Versand kaufen":

```
1. Artikel in Warenkorb legen
2. Warenkorb bestellen
3a) Versandadresse abfragen
3b) Bezahldetails abfragen
```

oder bei "eBook kaufen":

```
1. Artikel in Warenkorb legen
2. Warenkorb bestellen
3. Kaufabwicklung des Bezahldienstleisters starten
4. Downloadlink bereitstellen
```

![Vererbung des abstrakten Anwendungsfalls "Artikel kaufen"](plantuml/08_UseCaseVererbung.png)

Auch Anwendungsfälle kennen das konzept der Abstraktion: Im Beispiel kann "Artikel kaufen" selbst nicht ausgeführt werden, sondern modelliert nur ein Gerüst, das in konkreten Anwendungsfällen noch ausformulieren müssen.

Die Notation entspricht der für Vererbung (geschlossene Pfeilspitze) und Abstraktion (kursive Schrift, _constraint_ `{abstract}`) bekannten Darstellung.

## Wie viele Akteurinnen stehen mit wie vielen UseCases in Beziehung?

Um festlegen zu können, wie viele Akteurinnen für Anwendungsfälle nötig sind und an wie vielen Anwendungsfällen Akteurinnen beteiligt sind, werden Multiplizitäten an den Assoziationen angegeben, wie im UML-Klassendiagramm üblich und am Beispiel zu sehen:

![Multiplizitäten des Anwendungsfalls "Rundlauf" mit Spielerinnen und Schiedsrichterin](plantuml/07_Multiplizitaeten.png)

An einem Tischtennis-Rundlauf sind mindestens 3 Spielerinnen beteiligt, jede Spielerin  jedoch an exakt einem Rundlauf-Spiel. An einem Rundlaufspiel können keine oder beliebig viele Schiedsrichterin beteiligt sein. Jede Schiedsrichterin kann an höchstens einem Rundlaufspiel beteiligt sein.

Da diese Information jedoch häufig für die Adressaten des Use-Case-Diagramms keine Rolle spielt werden Multiplizitäten eher selten notiert.

## Gerichtete Assoziationen (initiiernde und sekundäre Akteurinnen)

In seltenen Fällen wird mit Hilfe von gerichteten Kanten dargestellt, welche Akteurinnen den Anwendungsfall aktiv triggern (primäre/initiierende Akteurinnen) und wer nur passiv vom Anwendungsfall benötigt wird (sekundäre Akteurinnen).


![Primäre und sekundäre Akteurinnen beim Anwendungsfall "Artikel kaufen"](plantuml/09_GerichteteBeziehungen.png)

## Weitere Literatur zu UML-Anwendungsfall-Diagrammen

- **als Primärquelle**: die UML Spezifikation der Object Management Group
Definition des Standards, jedoch nicht für Endnutzer aufbereitet): [https://www.omg.org/spec/UML](https://www.omg.org/spec/UML)

- **für den Einstieg**: Martina Siedl, Marion Brandsteidl, Christian Huemer, Gerti Kappel: UML@Classroom, dpunkt Verlag, Heidelberg 2012
Gut zu lesende Einführung in die wichtigsten UML-Diagramme, Empfehlung für den Einstieg.

- **für Lesefaule**: Die Vorlesungsreihe der Technischen Uni Wien (zu UML@Classroom) kann hier angeschaut werden (Videos, Folien): [http://www.uml.ac.at/de/lernen](http://www.uml.ac.at/de/lernen)

- **als Nachschlagewerk**: Christoph Kecher, Alexander Salvanos: UML 2.5 – Das umfassende Handbuch, Rheinwerk Bonn 2015, ISBN 978-3-8362-2977-7
Sehr umfangreiches Nachschlagewerk zu allen UML-Diagrammen

- **als Cheatsheet**: die Notationsübersicht von oose.de: [https://www.oose.de/wp-content/uploads/2012/05/UML-Notations%C3%BCbersicht-2.5.pdf](https://www.oose.de/wp-content/uploads/2012/05/UML-Notations%C3%BCbersicht-2.5.pdf)

- **UML und Software Engineering**: Chris Rupp, Stefan Queins & die SOPHISTen: UML2 glasklar, Hanser Verlag, München 2012; ISBN 978-3-446-43057-0 Schwerpunkt: Einbindung von UML-Diagrammen in den Softwareentwicklungszyklus

- **Zur Zertifizierungs-Vorbereitung**: M. Chonoles: OCUP 2 Certification Guide , Morgan Kaufmann Verlag, Cambridge 2018, ISBN 978-0-12-809640-6
    Informationen zur Zertifizierung nach OMG Certified UML Professional 2™ (OCUP 2™): Foundation Level

## Software zur Erzeugung von UML-Anwendungsfalldiagrammen

- [PlantUML](http://www.plantuml.com): Deklaratives UML-Tool, das in vielen Entwicklungsumgebungen integriert ist. auch als WebEditor verfügbar. Die obigen Diagramme wurden damit erzeugt - [Eine detaillierte Anleitung für plantUml mit den Quelltexten findet sich hier](https://oer-informatik.de/uml-usecase-plantuml)

- [WhiteStarUML](https://sourceforge.net/projects/whitestaruml/) (Relativ umfangreiches Tool, viele UML-Diagramme, mit Code-Generierung für Java, C, C# und Vorlagen für Entwurfsmuster)

- [Draw.io](http://www.draw.io): Online-Tool für FLowcharts usw - aber eben auch UML

## _Quellen und offene Ressourcen (OER)_

Die Ursprungstexte (als Markdown), Grafiken und zugrunde liegende Diagrammquelltexte finden sich (soweit möglich in weiterbearbeitbarer Form) in folgendem git-Repository:

[https://gitlab.com/oer-informatik/uml/umlanwendungsfall](https://gitlab.com/oer-informatik/uml/umlanwendungsfall).

Sofern nicht explizit anderweitig angegeben sind sie zur Nutzung als Open Education Resource (OER) unter Namensnennung (H. Stein, oer-informatik.de) freigegeben gemäß der [Creative Commons Namensnennung 4.0 International Lizenz (CC BY 4.0)](https://creativecommons.org/licenses/by/4.0/deed.de).

![Creative Commons Lizenzvertrag](https://i.creativecommons.org/l/by/4.0/88x31.png)
