# Anwendungsfall (Use Case)

"A use case is all the ways of using a system to achieve a particular goal for a particular user. Taken together the set of all the use cases gives you all of the useful ways to use the system, and illustrates the value that it will provide." (Ivar Jacobson) ^[Ivar Jacobson, der Initiator der UseCase-Diagramme, in "Use-Case 2.0": https://www.ivarjacobson.com/sites/default/files/field_iji_file/article/use-case_2_0_jan11.pdf]

Ein Anwendungsfall ist eine abgeschlossene Aufgabe des beschriebenen Systems. Jeder Anwendungsfall wird durch einen Akteur ausgelöst. Anwendungsfälle müssen so geschnitten werden, dass sie selbst einen eigenen Nutzen (ein messbares Ergebnis) produzieren.

Anwendungsfälle haben folgende Eigenschaften:
* Sie können nur funktionale Anforderungen beschreiben, die nicht funktionale Anforderungen müssen anderweitig dokumentiert werden.

* Sie sind für Anwender (Nutzer) in der Sprache der Problemdomäne und aus ihrer Perspektive geschrieben. Sie enthalten somit keine Implementierungsdetails.

* Sie dienen zur Abstimmung der Systemgrenzen und somit des Projektumfangs.

Anwendungsfälle bestehen aus Akteuren, Szenarien

## Was ist ein Akteur in Anwendungsfällen?

Akteure stehen außerhalb des modellierten Systems und tauschen Informationen mit diesem aus. Es kann sich um menschliche Benutzer oder andere Systeme handeln. Oft werden diese über Rollen dargestellt, die die Eigenschaften gegenüber dem System beschreiben (z.B. Admin, Anwender, Besucher).

## Was sind Szenarien in Anwendungsfällen?

Szenarien präzisieren Anwendungsfälle:

* Sie beschreiben aus Nutzersicht, was Akteure in einem Anwendungsfall tun, sehen, erleben.

* Die für den Anwendungsfall relevanten Systemmerkmale werden konkret, fokussiert und informell beschreiben.

* Szenarien enthalten keine Fallunterscheidungen ("Wenn...dann..."), da jeder Weg durch das System ein eigenes Szenario darstellt.

## Wie erstelle ich einen Anwendungsfall?

1. Identifizierung der Akteure:
  * Wer nutzt das System direkt (=> Hauptakteure) oder wird vom System indirekt unterstützt (=> Nebenakteure)?
  * Auf wen ist das System angewiesen, um die Anforderungen erfüllen zu können (z.B. Administration) (=> Nebenakteure)
  * Können diese Akteure in Gruppen/Rollen zusammengefasst werden?

2. Identifizierung der Szenarien:

  * Welche Szenarien führen die oben identifizierten Akteure aus?
  * Zu welchen Anwendungsfällen können diese Szenarien zusammengefasst werden?

## Tabellarische Darstellung eines Anwendungsfalls

|Name|Anzeige des Warenkorbs|
|--|--|
|**Kurzbeschreibung**|Stellt alle zum Kauf reservierten Artikel dar|
|**Hauptakteur**<br>löst den UseCase aus|Nutzer des Shops (angemeldeter Kunde oder Gast)|
|**Nebenakteur**<br>wird beim UseCase einbezogen|(Im konkreten Bespiel kein Nebenakteur, denkbar wäre im Bestellprozess z.B. ein Bezahldienstleister oder ein Versanddienstleister als Nebenakteur)|
|**Auslöser**<br/>Was macht der Hauptakteur, damit der Anwendungsfall getriggert wird?|Das Warenkorb-Symbol wird angeklickt|
|**Vorbedingung**<br/>Welche Voraussetzung müssen für einen fehlerfreien Ablauf gegeben sein?|Auswahl "gewerblich/privat" muss getroffen sein|
|**Nachbedingungen**<br/> kennzeichnen den Erfolgszustand / das Hauptszenario|Ausgabeseite mit der Liste aller reservierten Waren wird ausgegeben|
|**Fehlerzustand**<br/>kennzeichnen ein Nebenszenario|Dialoge, die weitere Daten erfordern (Land (VAT), Währung, privat/gewerblich) oder mit Infos zu aufgetretenen Problemen werden angezeigt |
|**Hauptszenario**|1. Der Nutzer klickt auf den Warenkorb<br/>2. das Pop-Up erscheint mit einer Zusammenfassung<br/>3. Der Nutzer klickt auf "Details anzeigen"<br/>4. Die Gesamtliste der erfassten reservierten Artikel wird ausgegeben<br/>5. die reservierte Menge, Preise, Nettosumme, Steuer, Bruttosumme und Versandkosten werden ausgegeben |
|**Nebenszenarien**<br/>die Abfolgen werden in gesonderten Listen erfasst - hier nur die Nennung|Leere Liste, Abfrage Währung, Abfrage privat/gewerblich, Cookies/Javascript nicht zugelassen, Artikel nicht mehr verfügbar, Preisänderung seit Bestellung, Artikel nicht gefunden |

Mit Hilfe des UML-Anwendungsfall-Diagramms lässt sich eine Übersicht aller Anwendungsfälle darstellen. Die Schritte einzelner Anwendungsfälle lassen sich beispielsweise mit dem UML-Aktivitätsdiagramm, dem UML-Sequenzdiagramm oder dem UML-Zustandsautomaten darstellen.

## _Quellen und offene Ressourcen (OER)_

Die Ursprungstexte (als Markdown), Grafiken und zugrunde liegende Diagrammquelltexte finden sich (soweit möglich in weiterbearbeitbarer Form) in folgendem git-Repository:

[https://gitlab.com/oer-informatik/uml/umlanwendungsfall](https://gitlab.com/oer-informatik/uml/umlanwendungsfall).

Sie sind bei Namensnennung (Hannes Stein) zur Nutzung als Open Education Resource (OER) freigegeben gemäß der [Creative Commons Namensnennung 4.0 International Lizenz (CC BY 4.0)](https://creativecommons.org/licenses/by/4.0/deed.de).

[![Creative Commons Lizenzvertrag](https://i.creativecommons.org/l/by/4.0/88x31.png)](https://creativecommons.org/licenses/by/4.0/deed.de)
