# UML Anwendungsfalldiagramm

* [UML Anwendungsfall-Diagramm: Einsatz und Notation](https://oer-informatik.gitlab.io/uml/umlanwendungsfall/uml-usecase.html) [(als PDF)](https://oer-informatik.gitlab.io/uml/umlanwendungsfall/uml-usecase.pdf)
* [Erstellung von UML Anwendungsfall-Diagrammen mit plantUML](https://oer-informatik.gitlab.io/uml/umlanwendungsfall/uml-usecase-plantuml.html) [(als PDF)](https://oer-informatik.gitlab.io/uml/umlanwendungsfall/uml-usecase-plantuml.pdf)

# Infos zur genutzten gitlab CI und deren Nachnutzung:
Die HTML und PDF-Dateien wurden mit Hilfe der Konfiguraiton der [TIBHannover](https://gitlab.com/TIBHannover/oer/course-metadata-test/) für die CI-Pipeline von Gitlab erstellt.
Die Vorlage findet sich hier: [https://gitlab.com/TIBHannover/oer/course-metadata-test/](https://gitlab.com/TIBHannover/oer/course-metadata-test/).
