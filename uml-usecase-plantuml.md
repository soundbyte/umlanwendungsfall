# Das UML Anwendungsfalldiagramm und die Modellierung mit plantUML

<span class="hidden-text">
https://oer-informatik.de/uml-usecase-plantuml
</span>

> **tl/dr;** _(ca. 7 min Lesezeit): Praktische Tipps, wie UML Anwendungsfalldiagramme mit Hilfe von [PlantUML](https://plantuml.com/de/) erstellt werden können. Ich versuche mich dabei näher an die UML-Spezifikation zu halten, als dies auf der PlantUML-Seite der Fall ist. Informationen zu UML-Anwendungsfalldiagrammen allgemein finden sich [in einem gesonderten Use-Case-Diagramm-Artikel](https://oer-informatik.de/uml-usecase)._

PlantUML ist ein Tool, um textbasiert deklarativ UML-Diagramme zu erstellen (oder, als Buzzword formuliert: _diagram as code_). Das bietet zwei große Vorteile:

* Die Diagramme sind einfach mit der Codebasis versionierbar, können jederzeit und von überall kurzfristig angepasst werden.

* Die Diagramme können direkt in die `Readme.md`-Dateien in den Repositories integriert werden, da relativ viele Markdown-Engines der gängigen `git`-Dienstleister diese unmittelbar rendern.

Ein großer Nachteil soll jedoch nicht verschwiegen werden: PlantUML ist recht störrisch, was die genaue Positionierung von Elementen angeht. Besser, man findet sich schnell damit ab, wo PlantUML das jeweilige Objekt hinrendert - sonst wird das schnell frustrierend.

Wie fängt man am besten an? Mit einem Beispiel-Diagramm. Und dem größten Vorteil: [dieser Link speichert das Diagramm und öffnet es direkt im Webeditor von planttext.com](https://www.planttext.com/?text=VLFBRjmm33nBNq7i9Ut1c_l2CB95YkOG147x1LgBiOMrgOKUoAlvzuhosckxQNqmB78u6fh36n-K2x7lP8TF0O85PvesW3OEws0iILK7ws2tfsSaZMu7oiDu69EF3diJUtY8uUqCEOUcSjqR6chvIqgcLzHqU2vAEeBnmHoBM-NRWrLEmxkKucfFZJeWQFI0Bm799r5mieQ25D5ZhJp2lMqCCVacVqaXYYAzIbEjIlokl3PLIlt49jtc3Bf8pGP0IaZFo3U9oPpVJOE4a14hiKDGqHioPfDF2oRuuTIfDRvkCrKwFV41CsWV_JEclq_DacQbvYvTPTt4iQfgfsoQ1mTkLM0f2ufVGVMzvaIc-8RA7T0mno0tpNA6cVKnJhOePZBMcQd7eIiUC8SlX1U-cP3jjXe6juEoD5HtKMDLPTIiu_-WBh_s7o0fHgEod6bexAgXONHem-i9f1WSL6w_F60ynbH2nS8YNu6fivjqPhwgBMd36m89_hSroGPCwtI25EeC7GSxJaMZ3z_7-pu-zUkdTfKYYG2-G8fPY-NlWdUuc5PlaJhltOS00V8cXNdd_m00). Im folgenden sind zu den meisten Diagrammen solche Links angegeben. Das ist aber nur ein Weg, plantuml zu bearbeiten. Für so ziemlich alle IDEs, git-Dienste, Editoren usw. gibt es plantUML-Plugins.

## Akteurinnen und Anwendungsfälle

Im Wesentlichen besteht ein Anwendungsfalldiagramm aus _Akteur*innen_ (`actor`), _Anwendungsfällen_ (`usecase`) und der _Systemgrenze_ (wird in plantUML mit `rectangle` deklariert). Ein minimales Anwendungsfalldiagramm sieht mit plantUML wie folgt aus:

```plantuml
@startuml
left to right direction
actor :Kundin: as customer

rectangle Shopsystem {

	usecase (Ware \n bestellen) as bestellen
	customer -- (informieren)
	customer -- bestellen
  }
@enduml
```

![Ein einfaches UseCase-Diagramm mit Akteurin, Systemgrenze und UseCase](plantuml/02_Systemgrenzen.png)

Codebeispiel: ( [online bearbeitbar](https://www.planttext.com/?text=fPAzJWCn3CVtF8N5qdRe2tJYeuAX5Kj128d5lNFlealioacesgflniQBuUEZ58a9CYN--Fbln-Sfe-OIWqkTvpKgHfZI2al8ryBv5YF1sRnKlaPO82UuswvfepwJdhcJdFj4LvXexy3EfMXDUYL15AQetI9WfijGwCEBLTUe5Au_8ZQjqLAKZjpXX7Wh-K-ukM_Q_4WXoCPu9y2BAel-M-0fK5Kslq7Su8QOr3J0haIOKoA6P-5-IkD-JEEfnyOGqUpaUOLLIpfnWLOPie3sBK3jbQhiXPt3NZfCPYR9ymGmGLLIbaXgjUoxFFi-4AmfM6nJfjWeyOxWzMLfffw3t0JQ7zpn3ejMrkaz6FREGKbKcJGOFg0IF34ioNmX48_wWiUNXGvcHNShqGZICjc-TC46ygaWVUr2chYZJ1YFOMYDMewtmV38_VHzqm_kdBZk5_8D) /  [vollständiger Quelltext](https://gitlab.com/oer-informatik/uml/umlanwendungsfall/-/blob/master/plantuml/02_Systemgrenzen.plantuml))

Der plantUML-Quelltext wird immer zwischen `@startuml` und `@enduml` eingefasst. Damit die Akteurinnen wie im UseCase-Diagramm üblich links des Systems stehen muss `left to right direction` voran gestellt werden. Der Übersichtlichkeit halber werden diese Befehle in den folgenden Codefragmenten ausgespart und sind nur unter den "Codebeispiel"-Links komplett einbezogen.

Akteurinnen werden ausserhalb der Systemgrenzen definiert und mit einem angezeigten Namen (hier: "Kundin") und einem Alias zur Referenzierung (hier: "customer") versehen.

`actor :Kundin: as customer`

Anwendungsfälle können gesondert deklariert und mit Alias versehen werden:

`usecase (Ware \n bestellen) as bestellen`

In Kurzform können Sie auch direkt bei Assoziationen erwähnt werden, sind dann allerdings nicht über Aliasse referenzierbar:

`customer -- (informieren)`

## Vererbung von Akteuren und "oder"-Beziehungen

Vererbungsbeziehungen zwischen Akteurinnen werden in PlantUML mit Hilfe ASCII-stilisierter Pfeilspitzen modelliert: die Operatoren `-|>` und `<|-`  symbolisieren jeweils die in Richtung der Generalisierung weisende Pfeilspitze.

`mitarbeiter <|- prokurist`

Ein komplettes UML-Diagramm wäre etwa wie folgt:

```plantuml
actor :Mitarbeiterin: as mitarbeiter
actor :Prokuristin: as prokurist

rectangle Vertragswesen {
  usecase (Vertrag \n ausfertigen) as ausfertigen
  usecase (Vertrag \n unterschreiben) as unterschreiben

  mitarbeiter -- ausfertigen
  prokurist -- unterschreiben
  mitarbeiter <|- prokurist
}
```

![Vererbungsbeziehung in einem Usecase-Diagramm](plantuml/03_VererbungVonAkteuren.png)
Codebeispiel: ( [online bearbeitbar](https://www.planttext.com/?text=fPBBJiCm44Nt_eeH6s2H7uXObBQ21QL2AY0XiPacayIA7zNO9gAb_uvDMt1HLv3Tl8xldSp8UMGVj1AkbsQ5Z1gcr61G_iOQFqTDS3OBbQmH5cWSFBwlQ63fYSz4DlFaQ8AEDWBYDxPS4q-iiWnJv7wXqDDO1TfLiUfRji7KkuwXYpIdRKzYcn7drly9DvDjvrz8AJj4NWQyhhpbVmlCWSnsE0KHvxTaYAC6M0SDCt9auCsQjAKYhQcOIcmZGiSyITDWrH6NGb7ZmLlWP05goLHvQOqGcAH3UI_ZprfIMhuq9Q03_PCvD3smxGDBv_SjgqCiHEAXQHN1Cx5dRDtm9MqZWgCgweABVG5U3M1mJOnaI-OoWRBuP7ymKOMhEYQvt8ySf-9UxWAl6yiQR6Se7bgluYQJHwZ97Ox8bIBp1KLnzFItdrJuzK0-TlLHPDwtOaIcJdV-2G00) /  [vollständiger Quelltext](https://gitlab.com/oer-informatik/uml/umlanwendungsfall/-/blob/master/plantuml/03_VererbungVonAkteuren.plantuml))

Die Reihenfolge der Akteurinnen legt fest, wie sie im Diagramm erscheinen. Die erstgenannte Akteurin erscheint oberhalb der zweitgenannten (sofern `left to right direction` aktiviert wurde - andernfalls erscheint die erstgenannte links):

`gf -|> befugte`

Abstraktion wird wie in der UML üblich mit kursiver Schreibweise oder über den constraint `{abstract}` notiert. Kursive Schreibweise wird über die HTML-Tags `<i>...</i>` realisiert. In den Akteurinnen-Namen können über das Steuerzeichen `\n` auch Zeilenumbrüche genutzt werden:

`actor :{abstract}\n<i>Befugte</i>: as befugte`

![Vererbungsbeziehung mit abstrakten Akteuren](plantuml/03_Vererbung_Abstrakt.png)

Codebeispiel: ( [online bearbeitbar](https://www.planttext.com/?text=fPBFJW8n4CRlVOe95_MmyKu8GI1o8SG4DJ7XCkpExZRRRScqTQF0s_WOtdWnM_x8OZnfRzFlww_VJ3km3jdvMWbRIRr2nXh6bADNxjveDyEQe3FrgSmGvgWjFBwjg67fY3kYTUR9qWWjhGM4DJIS4O-CCWnZv6gkqD5GUJgec5O56w-pWwCf0ytMfYAnRH5dnlq9DvL5wLv8ATC4NWjubph3_mxO1ZARvZU8k9gG9WuPuDtNC2LB6bwDZbDAufYIiSGY8EgmJrBdc9R4NQ4eT-0CS6m1CicKEccq41YZGtUDI-iu5Dk5xid-a79VEEhToduNqCBoK9_S3smgpz8wgVVowboVB1EoQRdxo9tDTvyb4n-jHIv4l1nreGYUYSEjXMtsVSG9UajfQ0EkZn8iD7WT_aJ0CSabwPk8kTmIuU0n8YJ9J_6izJR9HLO8SI3PzByDuHK6fBFuQRy0) /  [vollständiger Quelltext](https://gitlab.com/oer-informatik/uml/umlanwendungsfall/-/blob/master/plantuml/03_Vererbung_Abstrakt.plantuml))

Hier wurde eine Akteurin oberhalb und eine unterhalb positioniert, indem die Reihenfolge der Nennung und die Pfeilrichtung umgekehrt wurde.


## Vererbung von Anwendungsfällen

Die Syntax zur Vererbung von Anwendungsfällen ist analog zur der Syntax für Akteurinnen: wird ein Anwendungsfall "Artikel kaufen" durch die Anwendungsfälle "eBook kaufen" und "Buch per Versand kaufen" spezialisiert, so wird auch hier der stilisierte Pfeil `-|>` als Operator verwendet:

`eBookKaufen -|> ArtikelKaufen`

`ArtikelKaufen <|- BuchKaufen`

Ebenso können Usecases, die nicht eigenständig nutzbar sind, sondern nur Teilszenarien modellieren, die in anderen Usecases konkretisiert werden, als abstrakt geklariert werden. In PlantUML lässt sich dies wieder über die HTML-Tags `<i>...</i>` kursiv setzen oder über den Constraint `{abstract}` markieren. Neuere PlantUML-Versionen erkennen den Constraint und setzen den Usecasenamen automatisch kursiv (zeigen `{abstract}` selbst aber nicht mehr an).

```plantuml
usecase ({abstract}\n<i>Artikel kaufen</i>) as ArtikelKaufen
```

Ein Beispiel könnte etwa so aussehen.

```plantuml
actor :Kundin: as customer

rectangle Shopsystem {
  usecase ({abstract}\n<i>Artikel kaufen</i>) as ArtikelKaufen
  usecase (eBook kaufen) as eBookKaufen
  usecase (Buch per Versand kaufen) as BuchKaufen

  customer -- eBookKaufen
  customer -- BuchKaufen

  eBookKaufen -|> ArtikelKaufen
  ArtikelKaufen <|- BuchKaufen
}
```

![Vererbungsbeziehung mit abstrakten Akteuren](plantuml/08_UseCaseVererbung.png)

Codebeispiel: ( [online bearbeitbar](https://www.planttext.com/?text=fP8nRy8m48Lt_ueJIziXweuGWe3Q0SH2MwbIbsjo95OSNtIs6r7WlzS1gWRKgVNcT-zzVhOySHx5XzeeLsdRe60DSzfWCFw1hLzXJJ1OXapd26kq3fws3RMYFSb0zJBFZcReQASWhfGb9vcnOO4vIhKsw2arWKvJpAf2EDZyv6ZBI7CrLwGEFUAA_PzmIrsK_fMCuJRoUi1fvbd-NR0F5E7sDuYwUIHB4Zl0PwXXIOuiVB3jNYdfdYcPQomYeeuwQRl1h2GPAaCR3vv1kYj0heKohzagXLrr62vY9Ms7W0wou3pN94fr9hI58LYNtBYjyrJ3JWL7MJm9RdVuxhn4mk7DZlHuAbvNPA32i24xkjVZkuvtbXT7zITCANDrzXvzH-7QbOQiX8O4NaWSshmVw6Pd_tTdI98BJb_lsNiMIFRZgueNEnZjBw87DI6RTx_w2m00) /  [vollständiger Quelltext](https://gitlab.com/oer-informatik/uml/umlanwendungsfall/-/blob/master/plantuml/08_UseCaseVererbung.plantuml))


## Anwendungsfälle, die weitere Anwendungsfälle immer beinhalten

Um _include_-Beziehungen mit plantUML zu modellieren, also Anwendungsfälle, die die Funktionalität anderer Anwendungsfälle in jedem Fall enthalten - wird zum einen die Linienform und Pfeilspitze variiert, in dem zwischen den Anwendungsfällen der Operator `.>` notiert wird, zum anderen wird die neben den Pfeil das Sterotyp _&laquo;include&raquo;_ notiert:

`ausfertigen .> drucken : <<include>>`

![Vererbungsbeziehung mit abstrakten Akteuren](plantuml/05_include.png)

Codebeispiel: ( [online bearbeitbar](https://www.planttext.com/?text=fP91RzGm48Nl_XMZNXOEom-8gbLfLt1PUbb0Gk8oZTzchJZZQ6mJbOh_pfYkH93WKdAAny_VFB_nJIwifKxHvJ78pCeJxN7Y6ikx9EMU9z3LeVR1CnrPCdrydB5eAD0hjphpAUEECvuSsNURr4FlKanAUzRn6BdWDbOyxt8_3fgg-6V5SZPQdj88zsD5l4_bHRX36Cxb2s9CY_5MmBTzIVhV1jT0rRJy3U8sxo5Gyq3VwqG7P0XzIz9IshQOjll0Wo4cgoF8YVipj7CHfq8baROha0-AleGapd6pJjs7OCDwG0i_I4USQVfTSQx9MOO8-WmjoaDUVdL-SZMZjxRqwh91NuMuvfEjmW1vtL2hzR_qNci_NkINV-TM5cYx_GFZDli06f2B1hjXBB3fY9MMe9vcE_1cH-OO0nvClh74oHlxQ2oayZYJM3BaOMyopJFYN6KidLlrQ8IB5Uhe-Zf87wl7RcUZke7uzh9_0W00) /  [vollständiger Quelltext](https://gitlab.com/oer-informatik/uml/umlanwendungsfall/-/blob/master/plantuml/05_include.plantuml))


```plantuml
actor :Mitarbeiterin: as mitarbeiter

rectangle Vertragswesen {
	usecase (Vertrag \n ausfertigen) as ausfertigen
	usecase (Vertrag \n ausdrucken) as drucken

	mitarbeiter -- ausfertigen

	'Die gestrichelte Linie wird per .> angegeben
	'und das Stereotyp nach dem Doppelpunkt:
	ausfertigen .> drucken : <<include>>
}
```

## Anwendungsfälle, die unter Umständen durch weitere Anwendungsfälle erweitert werden

_Extend_-Beziehungen, bei denen Anwendungsfälle nur unter bestimmten Umständen um die Funktionalitäten eines zweiten Anwendungsfalls erweitert werden, können in PlantUML nur über ein paar Tricks modelliert werden. Analog zu _include_-Beziehungen wird der gestrichtelte Pfeil und der zugehörige Stereotyp &laquo;extend&raquo; notiert:

`bestellen <.(Neukunden-Registrierung) : <<extend>>`

Zur genauen Zurodnung der Bedingung, unter der die Erweiterung gilt, muss jedoch zusätzlich noch ein _extension point_ und eine _condition_ angegeben werden.

Der Extention Point wird zum einen direkt in der Ellipse des Anwendungsfalls referenziert:

```plantuml
usecase bestellen as "bestellen
  --
  <i>extension points:</i>
  Registrierung"  
```

Zum anderen muss er - mit zugehöriger Bedingung (_condition_) ein einer Notiz notiert werden, die mit der zugehörigen Assoziation verbunden ist.

```plantuml
bestellen <.(Neukunden-Registrierung) : <<extend>>

note top on link
  <i>condition:</i>
  {Neukunde}
  <i>extension point:</i>
  Registrierung
end note
```

![Extend-Beziehungen mit PlantUML modellieren](plantuml/06_extend.png)

Codebeispiel: ( [online bearbeitbar](https://www.planttext.com/?text=fPDFZzCm4CNFdaynwgLmo7AFech_LEo1gaWK3XozoJIrueoZiKrPgdvtnbJTBT0Jv639u_5ltdjoRcEoalBe8GwE9ojsn2LjRVRfVU2qjYFXR9LRrrdSM8xu-NcYlRX4CeEBEryYFTX81q3zxeDq90_11y6bbM7ZRQ9xd-bqQjkXbv2vEtNiTqgBOnW8ZXV4TKZ_X5kvVfU-alTXhxmBu5sRWlotm4kWIDXVWy3yaPX4DU2FFEAA8Z5-2rnIgafCrTBPNX6ZridnrhOxaXeyRHEcW58iOEU4skG20zWY7UiFAibnZJPYcsCA8mb0QRBSUyAFx1rJt8K93s3cTzpR9yAE8jBtH1mLXLDmdC3aIAtgm2UAIMMg36NENdPWJ5NfqhZ57rTZtRnp2ptwHBsBIHn9vdw6263Ech2gNi5QdslMkD4NGo4zJmZcTMfpysPDUL1VnDLloBTOOzFycjuj5eNI1dLVya1j_ziIwtlHy2RKakOmdEI_N3hhFfpd7Q_wkseFZ8h0CW2E1Wt2hUxBF_CJ) /  [vollständiger Quelltext](https://gitlab.com/oer-informatik/uml/umlanwendungsfall/-/blob/master/plantuml/06_extend.plantuml))

## Wie viele Akteurinnen stehen mit wie vielen UseCases in Beziehung?

Multiplizitäten werden in PlantUML diretk bei den Assoziationen und in Anführungszeichen notiert. Es empfiehlt sich, längere Verbindungslinien zwischen Akteurinnen und Anwendunsfällen zu zeichnen (über drei Bindestriche: `---`), damit die Zahlen weit genug auseinander liegen und leichter zugeordnet werden können.

`player "3..*" --- "1" rundlauf`

![Multiplizitäten in Use-Case-Diagrammen](plantuml/07_Multiplizitaeten.png)


Codebeispiel: ( [online bearbeitbar](https://www.planttext.com/?text=fPBFJiCm3CRFv2ciNlWZ5O6uxRID2IvZ1mO7ZYPrMskfKpafrOPu6zw45oDbYFN02N99Od_-wRFbQOYeiMkS2HkM5XKRM529dOitNk8A6u9isLak4DOe0HwsBVNAaJGpevh7GDSOwDL0EdEl1UcrTrvXWRfPEumqTntjisWtbVfEYhsYhnCjD7v3vcr4NFdu9zoIgpe-aNE-JxmHS6QZrtyR70DLVVyRn1pVaf0c3x3h6bXI884NByEKyc5C-OAnIeWcnOcbH5kJJeoZCaBqe4CBKB2IZUp561oimsJTCZbIbWbWWDRXblGdPskc8YZRElv8bCfa8naQI2YL8tZWC2X4EC2hwGBPv0TExbEt3hlozAli-vCQkKkBmAtZ7SUFzrG6FQNH2Q30SCr2y3l6w5c-WXLLz4nYzjuWkpe_FyiWptF8BhC3zziNP1U7V7gE9Mz70CPCIOfXEJy1) /  [vollständiger Quelltext](https://gitlab.com/oer-informatik/uml/umlanwendungsfall/-/blob/master/plantuml/07_Multiplizitaeten.plantuml))



```plantuml
actor :Spielerin: as player
actor :Schiedsrichterin: as referee

rectangle Tischtennis {
	usecase (Rundlauf) as rundlauf

	'Multiplizitäten werden an den Assoziationen angegeben
	player "3..*" --- "1" rundlauf
	referee "0..1" --- "0..*" rundlauf
}	  
```

## Gerichtete Assoziationen (initiiernde und sekundäre Akteurinnen)

Für die seltenen Fällen, in denen zwischen initiierenden und sekundären Akteurinnen unterschieden werden muss, können in PlantUML die Operatoren `--->` genutzt werden, um eine gerichtete Assoziation zu zeichnen.


![Gerichtete Assoziationen](plantuml/09_GerichteteBeziehungen.png)

Codebeispiel: ( [online bearbeitbar](https://www.planttext.com/?text=fPAzJiGm3CTdyXJMBS3G5xW1tPTWu7GC1mEZQTqsIkgS78UAE_7kf2r87PWWG-JuuvU_dQoYecZgl8dEyWa5EzXHZSdhVM0zO4UmsAVILWX7v0ZF7oVgnIh9mingNY9jCTB5G5wR81N9DlWWi4DnHuzA6vzeYcBf6Wc9gocZRpCjTi6H-PmH3q7_XDlRfjLNyZxqcJS3haiDyc-1Sw18w7-3cEi7OfAi0SwfWpr5OdWFF4of6CPKx2ms6T5bFrckiMn9biPJhQ01P6W1AYjKgWri30xIORcXCxQ-iiHHFTcO7s496E5jzFzaFMRXbiU06qqpS90RJ_34tZ8Tst22YxbAaSgi1cxMejQH1uUf9huTIxEPsvq8K1HtasVOnjFtdMP5N0tVvmi0) /  [vollständiger Quelltext](https://gitlab.com/oer-informatik/uml/umlanwendungsfall/-/blob/master/plantuml/09_GerichteteBeziehungen.plantuml))


```plantuml
actor :Bezahldienstleister: as bezahl
actor :Kundin: as kundin
rectangle OnlineShop {
	usecase (Artikel kaufen) as kauf
}
kundin --> kauf
kauf --> bezahl
```


## plantUML-Formatierung: Ausrichtung der Linien durch einfache oder zweifache Zeichen (- / -- / . / ..)

PlantUML bietet die Möglichkeit Assoziationsrichtungen vorzugeben über die Operantoren -up-, -down-, -left-, -down-. Wenn man jedoch die Programme mit der Option "left to right direction" nutzt sind durch die Drehung sämliche Richtungsanweisungen verkehrt...

Als Alternative kann man auch die Notation mit einfachen und doppelten Bindestrichen wählen:

![](plantuml/04_Ausrichtung.png)

```plantuml
'wie immer wurde die Richtung gedreht:
left to right direction

actor :Personaler: as personaler

'nach oben dann mit nur einem Bindestrich:
(Kündigen aussprechen \n <i> oben </i>)- personaler

'nach unten durch vertauschen von Akteurin und UseCase
personaler - (Bewerbungsgespräche führen  \n <i> unten </i>)

'seitlich nach rechts mit zwei Strichen
personaler -- (Beförderungsverfahren eröffnen  \n <i> rechts </i>)

'seitlich nach links mit Vertauschten Positionen
(Abmahnungnen aussprechen \n <i> links </i>) -- personaler
```

## plantUML-Formatierung: Aufhübschen von  Anwendungsfall-Diagrammen

Wenn die Diagramme erstmal stehen will man sie aufhübschen. Dafür stehen allerlei möglichkeiten zur Verfügung, die v.a. auf der plantUML-Seite dargestellt werden. Einige Beispiele sind hier abgebildet:



#### Minimalbeispiel: nur die Leserichtung wurde angepasst:

![](plantuml/10_Aufhuebschen1.png)

```plantuml
@startuml

left to right direction

actor :Akteur:
rectangle System {
usecase Anwendungsfall  
Akteur -- Anwendungsfall
note top on link
Notiz...
end note
}
@enduml
```

#### Anpassung von Schriftart und Farben

![](plantuml/10_Aufhuebschen2.png)

```plantuml
@startuml

skinparam DefaultFontName "Lucida Sans Typewriter"
skinparam UseCase{
 BorderColor DarkSlateBlue
 BackgroundColor whitesmoke
}
skinparam Note{
 BorderColor DarkSlateBlue
 BackgroundColor LightYellow
}
skinparam Actor{
 BorderColor DarkSlateBlue
 BackgroundColor whitesmoke
}
skinparam ArrowColor DarkSlateBlue
left to right direction

actor :Akteur:
rectangle System {
usecase Anwendungsfall  
Akteur -- Anwendungsfall
note top on link
Notiz...
end note
}
@enduml
```


#### Sieht nach Entwurf aus: um die Vorläufigkeit und Änderbarkeit zu unterstreichen kann man es nach einer Skizze aussehen lassen.

![](plantuml/10_Aufhuebschen3.png)


```plantuml
@startuml

' Welche Schriften gibt es auf dem System?
' listfonts als plantUML-Kommando gibt's aus.
skinparam DefaultFontName "FG Virgil"
skinparam handwritten true
skinparam monochrome true
skinparam packageStyle rect
skinparam shadowing false

left to right direction

actor :Akteur:
rectangle System {
usecase Anwendungsfall  
Akteur -- Anwendungsfall
note top on link
Notiz...
end note
}
@enduml
```

## Weitere Literatur zu UML-Use-Case-Diagrammen

- [OER-Informatik Artikel zu Use-Case-Diagrammen allgemein](https://oer-informatik.de/uml-usecase)

- **als Primärquelle**: die UML Spezifikation der Object Management Group
Definition des Standards, jedoch nicht für Endnutzer aufbereitet): [https://www.omg.org/spec/UML](https://www.omg.org/spec/UML)

- **für den Einstieg**: Martina Siedl, Marion Brandsteidl, Christian Huemer, Gerti Kappel: UML@Classroom, dpunkt Verlag, Heidelberg 2012
Gut zu lesende Einführung in die wichtigsten UML-Diagramme, Empfehlung für den Einstieg.

- **für Lesefaule**: Die Vorlesungsreihe der Technischen Uni Wien (zu UML@Classroom) kann hier angeschaut werden (Videos, Folien): [http://www.uml.ac.at/de/lernen](http://www.uml.ac.at/de/lernen)

- **als Nachschlagewerk**: Christoph Kecher, Alexander Salvanos: UML 2.5 – Das umfassende Handbuch, Rheinwerk Bonn 2015, ISBN 978-3-8362-2977-7
Sehr umfangreiches Nachschlagewerk zu allen UML-Diagrammen

- **als Cheatsheet**: die Notationsübersicht von oose.de: [https://www.oose.de/wp-content/uploads/2012/05/UML-Notations%C3%BCbersicht-2.5.pdf](https://www.oose.de/wp-content/uploads/2012/05/UML-Notations%C3%BCbersicht-2.5.pdf)

- **UML und Software Engineering**: Chris Rupp, Stefan Queins & die SOPHISTen: UML2 glasklar, Hanser Verlag, München 2012; ISBN 978-3-446-43057-0 Schwerpunkt: Einbindung von UML-Diagrammen in den Softwareentwicklungszyklus

- **Zur Zertifizierungs-Vorbereitung**: M. Chonoles: OCUP 2 Certification Guide , Morgan Kaufmann Verlag, Cambridge 2018, ISBN 978-0-12-809640-6 (Informationen zur Zertifizierung nach OMG Certified UML Professional 2™ (OCUP 2™): Foundation Level)

## Software zur Erzeugung von UML-Use-Case-Diagrammen

- [PlantUML](http://www.plantuml.com): Deklaratives UML-Tool, das in vielen Entwicklungsumgebungen integriert ist. auch als WebEditor verfügbar. Die obigen Diagramme wurden damit erzeugt

- [WhiteStarUML](https://sourceforge.net/projects/whitestaruml/) (Relativ umfangreiches Tool, viele UML-Diagramme, mit Code-Generierung für Java, C, C# und Vorlagen für Entwurfsmuster)

- [Draw.io](http://www.draw.io): Online-Tool für Flowcharts usw. - aber eben auch UML


## _Quellen und offene Ressourcen (OER)_

Die Ursprungstexte (als Markdown), Grafiken und zugrunde liegende Diagrammquelltexte finden sich (soweit möglich in weiterbearbeitbarer Form) in folgendem git-Repository:

[https://gitlab.com/oer-informatik/uml/umlanwendungsfall](https://gitlab.com/oer-informatik/uml/umlanwendungsfall).

Sofern nicht explizit anderweitig angegeben sind sie zur Nutzung als Open Education Resource (OER) unter Namensnennung (H. Stein, oer-informatik.de) freigegeben gemäß der [Creative Commons Namensnennung 4.0 International Lizenz (CC BY 4.0)](https://creativecommons.org/licenses/by/4.0/deed.de).

![Creative Commons Lizenzvertrag](https://i.creativecommons.org/l/by/4.0/88x31.png)
