# Anforderungsanalyse (_requirement engineering_)

> **tl/dr;** _(ca. 11 min Lesezeit): Welche Techniken gibt es, um Anforderungen zu ermitteln, Dokumentieren, Gliedern, Validieren und Messbar zu machen? Welche Kriterien werden an gute fachgerechte und überprüfbare Anforderungen angelegt?_

## Wozu dient die Anforderungsanalyse?

Projekte verfolgen bestimmte Ziele, die sie erreichen sollen - diese Ziele sind zum Teil offensichtlich, zum Teil versteckt.  Unabhängig vom gewählten Vorgehensmodell müssen diese Ziele (die Anforderungen an ein System) gesammelt und verstanden werden.

_Anforderungen_ definiert das _International Requirement Engineering Board_ (IREB) als: ^[[Glossar 2.0 des IREB](https://www.ireb.org/content/downloads/2-cpre-glossary-2-0/ireb_cpre_glossary_de_2.0.1.pdf)]

    A need perceived by a stakeholder.
    A capability or property that a system shall have.
    A documented representation of a need, capability or property

Diese Anforderungen müssen systematisch erhoben, dokumentiert, validiert und verwaltet werden. Diesen Prozess nennt man _Requirement Engineering_. Eine Übersetzung dafür könnte etwa Anforderungsanalyse sein. Was versteht man unter Anforderungsanalyse?

* In der Anforderungsanalyse wird versucht zu verstehen und zu beschreiben, was die AnwenderInnen / Stakeholder (Interesseneigner*innen / Betroffene) vom zu erstellenden System wünschen oder brauchen.

* Die Aufgabe eines Software-Systems werden in seinem Nutzungskontext identifiziert und dokumentiert.

* Fähigkeiten der Software sowie der erforderlichen Randbedingungen werden beschrieben.

Der Aufwand, der für die Anforderungsanalyse eingeplant werden sollte, hängt von vielen Einflussfaktoren ab:

* Je größer das Risiko des Scheiterns eines Projekts und je größer der zu erwartende Schaden eines Scheiterns um so genauer sollte die Anforderungsanalyse durchgeführt werden.

* Je größer und diverser die Gruppe der _Stakeholder_, desto mehr Zeit sollte in das Verständnis der jeweiligen Anforderungen investiert werden.

## Welche Arten von Anforderungen gibt es?

Eine systematische Analyse verhindert, dass wesentliche Anforderungen vergessen werden. In welchen Bereichen Anforderungen ermittelt werden können, gibt beispielsweise die **FURPS+**-Systematik wieder:

![FURPS - Abkürzung](images/FURPS.png)

FURPS+ steht hierbei als Akronym für:

* _**F**unctional_: Angemessenheit, Sicherheit, Interoperabilität, Konformität, Ordnungsmäßigkeit, Features, Fähigkeiten

* _**U**sability_ / Benutzerfreundlichkeit:  Human factors, Dokumentation, Attraktivität, Bedienbarkeit, Erlernbarkeit, Konformität, Verständlichkeit

* _**R**eliability_ / Zuverlässigkeit: Häufigkeit von Fehlern, Fehlererholung, Stabilität, Testbarkeit, Reife, Wiederherstellbarkeit, Sicherheit

* _**P**erformance_ / Effizienz: Antwortzeiten, Durchsatz, Verfügbarkeit, Kosten, Verbrauchsverhalten

* _**S**upportability_ / Wartbarkeit: Analysierbarkeit, Modifizierbarkeit, Internationalisierbarkeit, Skalierbarkeit

* "**+**" steht für weitere Anforderungen wie: _Implementation_ (Endliche Resourcen, Sprachen und Tools, Hardware), _Interface_ (Schnittstellen für andere Systeme), _Operations_ (Systemmanagement), _Packaging_ (Strukturierung), _Legal_ (Lizenzen usw.)

Die funktionalen Anforderungen unterscheiden sich wesentlich von den anderen Anforderungen, daher wird grob unterteilt in funktionale Anforderungen (`F`)  und Qualitätsanforderungen (nicht funktionalen Anforderungen, `URPS+`). Bei der Anforderungsanalyse werden diese Gruppen noch ergänzt durch die Randbedingungen.

### Funktionale Anforderungen (_Functional requirements_)

Das IREB fasst funktionale Anforderungen so zusammen:

_A requirement concerning a result or behavior that shall be provided by a function of a system._^[[IREB-Glossar zur Funktionalen Anforderung](https://www.ireb.org/content/downloads/2-cpre-glossary-2-0/ireb_cpre_glossary_de_2.0.1.pdf)]

Da funktionale Anforderungen die Fähigkeiten des Systems beschreiben ist sowohl ihre Formulierung als auch die Zielüberprüfung relativ leicht objektivierbar. Sie können stark strukturiert oder natürlichsprachlich dokumentiert werden, beispielsweise in Form von Use-Cases (siehe unten).

### Qualitätsanforderungen (_Quality requirements_)

Die nichtfunktionalen Anforderungen (Qualitätsanforderungen) sind häufig nicht offensichtlich, schwerer formulierbar und deren Zielerreichung oft sehr schwer objektivierbar. Da Qualitätsanforderungen bei der Abnahme und der Kundenzufriedenheit eine große Rolle spielen sind Sie für den Projekterfolg sehr wichtig. Häufig werden sie lediglich mit Absichtserklärungen ("soll leicht bedienbar sein") und weichen, nicht überprüfbaren  Formulierungen festgeschrieben ("intuitive Benutzeroberfläche"), die bei Abnahme zu Streitigkeiten und Unzufriedenheit führen können. Daher sollten wo immer möglich statt qualitativer Akzeptanzbedingungen ("Dass System muss performant sein.") in den Anforderungen überprüfbare und quantifizierbare Akzeptanzkriterien beschrieben werden ("Der Importvorgang einer Jahresrechnung darf nicht länger als 15 Sekunden dauern.").

Das IREB beschreibt die Qualitätsanforderungen so:

_A requirement that pertains to a quality concern that is not covered by functional requirements._^[[IREB-Glossar zur Qualitätsanforderung](https://www.ireb.org/content/downloads/2-cpre-glossary-2-0/ireb_cpre_glossary_de_2.0.1.pdf)]

### Randbedingungen (_Constraints_)

Randbedingungen stellen keine Anforderungen dar, die das Projekt erreichen muss, sondern schränken die Realisierung des Projekts ein:

_A requirement that limits the solution space beyond what is necessary for meeting the given functional requirements and quality requirements_^[[IREB-Glossar zur Randbedingungen](https://www.ireb.org/content/downloads/2-cpre-glossary-2-0/ireb_cpre_glossary_de_2.0.1.pdf)]

Diese Einschränkungen können technischer, rechtlicher, organisatorischer, kultureller oder umweltbezogener Natur sein - nicht immer sind sie zum Projektstart bereits bekannt.

### Andere Unterteilungen

Eine ähnliche Gliederung wie FURPS wird in der ISO/IEC 25000 (vormals ISO/IEC 9126) zur Beschreibung von Softwarequalität genutzt. Alle gängigen Systematiken helfen dabei, dass keine Anforderungsbereiche vergessen werden.

## Phasen der Anforderungsanalyse (_requirements engineering_)

Anforderungen müssen über das Gesamtprojekt gemanagt werden, hierzu gibt es unterschiedliche Vorgehensweisen und Gliederungen in Phasen, die das _International Requirements Engineering Board_ beispielsweise so unterteilt:

![Phasen des Requirement Engineering nach dem International Requirements Engineering Board](images/RequirementEngineering.png)

* Phase 1: Ermitteln der Anforderungen (_requirements elicitation_)

*	Phase 2: Dokumentieren der Anforderungen (_requirements documentation / specification_)

*	Phase 3: Prüfen und Bewerten der Anforderungen (_requirements validation_)

* Phase 4: Verwalten der Anforderungen, Anpassen und Ändern von Anforderungen im Prozess (_requirements management_)

Im gesamten Projektverlauf müssen die drei ineinander verzahnten Fragen beantwortet werden:

*  Was ist das Ausgangsproblem?

* Welche Anforderungen ergeben sich daraus?

* Welche Lösungen können diese Anforderungen erfüllen?

## Phase 1: Anforderungsermittlung (requirements elicitation)

Woher kommen die Anforderungen an das System, mit welchen Techniken kann ich Anforderungen erheben und ermitteln? Durch systematisches Vorgehen können Anforderungen gewonnen, detailliert und verfeinert werden. Je nach Projektumfeld und _Stakeholder_ sollten dabei möglichst mehrere Techniken und unterschiedliche Gruppen einbezogen werden.


* Interview
  Ein offenes Gespräch ohne festgelegten geschlossenen Fragenkatalog mit unterschiedlichen _Stakeholern_

* Umfrage (zuvor definierte Fragen) mit unterschiedlichen _Stakeholern_

* Workshop / Meeting / Brainstorming / Rollenspiele mit

  * Entwicklerteam

  * Mitarbeitern der Fachdomäne

  * anderen Stakeholdern

* Analyse der bestehenden Geschäftsprozesse über:

  * Anwender-Beobachtung

  * Analyse der Dokumentation von Geschäftsprozessen (z.B. Dokumente aus dem Qualitätsmanagement beteiligter Unternehmen, EPK, Flussdiagramme)

* Marktanalyse:

  * Gibt es bereits Konkurrenzprodukte? Dann können diese direkt analysiert werden

  * In welchen Bereichen gibt es vergleichbare / ähnliche Geschäftsprozesse, die analysiert werden können?

Wichtig ist bei der Anforderungsermittlung insbesondere, dass der Blick aufs Ganze erweitert wurde:

* Wer sind die _Stakeholder_ (also diejenigen, die ein Interesse an dem System haben oder von diesem betroffen sind)?
Wer ist in die Geschäftsprozesse eingebunden: wer sind die Auftraggeber, Kunden, Anwender, Mitarbeiter der Fachdomäne, Kontrollorganisationen, Verantwortliche, Ausführende usw. ?

* Welche Anforderungen haben diese _Stakeholder_ an das System?

* Haben alle Projektbeteiligten ein gemeinsames Verständnis des Systems, haben sie eine gemeinsame Sprache / ein einheitliches Vokabular und meinen alle mit jedem Begriff das gleiche?

* Welche konkurrierenden / widersprüchlichen Anforderungen gibt es?

* Wurde das System im Gesamtkontext mit den umgebenden anderen Systemen betrachtet?

* Welche Randbedingungen werden angenommen (muss zusätzlich erfasst werden!)?  Randbedingungen können z.B. technischer, rechtlicher, kultureller, organisatorischer, ökologischer oder physikalischer Natur sein (keine abschließende Liste!).

* Wie können sich diese Randbedingungen realistischerweise ändern?


## Phase 2: Dokumentieren der Anforderungen (_requirements documentation_)

Je nach Vorgehensmodell und Granularität (Detailgrad) der erfassten Anforderungen können sich die Dokumentationsmittel unterscheiden.

### Übersicht von Anforderungen

Um das _big picture_ von Anforderungen zu erhalten bieten sich beispielsweise folgende Artefakte an:

* Wenn Anforderungen von Beginn an relativ fest stehen können oft textuelle Auflistung der Anforderungen vorab erfolgen:

  *   In diesen Anforderungsdokumenten können  funktionale und nicht-funktionale Anforderungen festgeschrieben werden. Der Grad an Formalität kann von Stichpunktlisten bis hin zu streng formellen Dokumenten reichen.

  * Das **Lastenheft** wird durch Auftraggeber erstellt: Was soll umgesetzt werden und warum? Es enthält oft noch keine technische Sicht und ist in der Sprache der Problemdomäne formuliert.

  * Im **Pflichtenheft** erarbeitet der Auftragnehmer wie und womit die Anforderungen des Lastenhefts umgesetzt werden sollen. Es beschreibt Werkzeuge und (grobe) Entwürfe und nutzt die Sprache der Lösungsdomäne. Das Pflichtenheft wird i.d.R. Vertragsbestandteil und sollte mit entsprechender Sorgfalt erstellt werden.

* [Anwendungsfälle](https://oer-informatik.gitlab.io/uml/umlanwendungsfall/anwendungsfall.html)
  * "A use case is all the ways of using a system to achieve a particular goal for a particular user. Taken together the set of all the use cases gives you all of the useful ways to use the system, and illustrates the value that it will provide." (Ivar Jacobson) ^[Ivar Jacobson, der Initiator der UseCase-Diagramme, in "Use-Case 2.0": https://www.ivarjacobson.com/sites/default/files/field_iji_file/article/use-case_2_0_jan11.pdf]
  * Anwendungsfälle können in **[UML _Use-Case_-Diagrammen](https://oer-informatik.gitlab.io/uml/umlanwendungsfall/uml-usecase.html)** (Anwendungsfalldiagramm) graphisch und übersichtlich dargestellt werden

  ![UML Use-Case-Diagramm](plantuml/11_Beispiel.png)

  * Mit Hilfe dieser einfachen grafischen Darstellungsform können Anwendungsfälle dialogisch mit _Stakeholdern_ erarbeitet werden.
  * In Anwendungsfällen werden funktionale Anforderungen zusammengefasst, die gemeinsam von einem Akteur initiiert werden und dem Anwender einen Nutzen generieren.
  * Die einzelnen Anwendungsfälle können im nächsten Schritt [tabellarisch präzisiert](https://oer-informatik.gitlab.io/uml/umlanwendungsfall/anwendungsfall.html) werden. Hierbei wird ein Name, eine kurze Beschreibung, ein auslösender Akteur, Randbedingungen sowie Haupt- und Nebenszenarien erfasst.
  * Einzelne Wege durch den Anwendungsfall werden als Szenarien definiert und als Stichpunktliste oder ggf. als UML Aktivitäts-, Zustands- oder Sequenzdiagramm entworfen.

* _**Product Backlog**_:
Vor allem im agilen Umfeld werden die Anforderungen häufig als einzelne isolierte Tasks gesammelt. Diese Tasks sollten - unabhängig von der genutzten Technik - so formuliert werden, dass sie den I.N.V.E.S.T.-Kriterien genügen:

|Name |Beschreibung |
|:---|:---|
|I:Independent|Jeder Task muss unabhängig von anderen Tasks umgesetzt werden können. Er sollte frei von Abhängigkeiten sein.|
|N:Negotiable|Nichts ist in Stein gemeißelt. Der Inhalt eines Tasks ist im Team verhandelbar: sie können geteilt, geändert, zusammengelegt... werden.|
|V:Valueable|Jeder Task produziert aus sich heraus einen Nutzen und hat einen Wert für den Anwender.|
|E:Estimatable|Das Team muss in der Lage sein, abschätzen zu können, wie lange es für jeden Task etwa benötigt. Erscheint die Schätzung nicht möglich ist der Task vermutlich zu groß.|
|S:Small|Tasks müssen die richtige Größe haben: zu große Tasks sollten unterteilt werden. Ziel muss es sein, dass jeder Task am Ende eines Tages in das Repository übernommen werden kann.|
|T:Testable|Alle Daten, die zum Testen des Tasks nötig sind, müssen genannt sein. Im einfachsten Fall sind dies Akzeptanzkriterien.|

Es kann hilfreich sein zur Formulierung der Anforderungen auf Satzschablonen zuzugreifen, um die Formulierungen einfach und präzise zu halten. Die bekannteste Satzschablone stellen die **User Stories** dar, die festlegen, dass Rolle des Nutzers, Ziel der Anforderung und der Nutzen für die Anwender genannt werden müssen:

``Als ROLLE möchte ich ZIEL um zu NUTZEN``

### Welche Informationen sollten zu Anforderungen erfasst werden?

Im Idealfall wird für jede Anforderung bereits im Vorfeld erfasst:

* Name der Anforderung / des Tasks

* Beschreibung (ggf. in Form von Satzschablonen)

* Akzeptanzkriterium: Messbare und mit Stakeholdern abgestimmte Bedingung, wann eine Anforderung als erfüllt gilt (besonders bei nicht funktionalen Anforderungen wichtig!)

* Grundlegende Testfälle (Eingabeparameter, Randbedingungen, erwartetes Ergebnis)

* Risiko bei Nichterfüllung / fehlerhafter Erfüllung

* Geschätzter Aufwand

### Festlegung von Akzeptanzkriterien bei Qualitätsanforderungen

Nach der FURPS-Methodik ist die Beschreibung von Akzeptanzkriterien v.a. bei den Qualitäts- (also  nicht funktionalen) Anforderungen schwierig. Es sollten nach Möglichkeit Metriken entwickelt und festgeschrieben werden, die die Erfüllung der Anforderung objektivieren. Ein paar Beispiele als Anhaltspunkt:

* U wie Usability:
Ein Nutzer mit gegebenem Vorwissen muss eine definierte Aufgabe ohne Hilfe in einer definierten Zeitspanne abarbeiten können. Die Zeit wird gestoppt und das Verhalten des Nutzers beobachtet.

* R wie Reliability:
Das System wird mit einem Smoketest bewusst an die Belastungsgrenze gebracht (definierte Anzahl an Zugriffen pro Zeiteinheit). Die akzeptablen Anwortzeiten oder verlorenen Pakete / Neustarts werden festgeschrieben. Informationen zu Abstürzen werden den Logfiles entnommen.

* P wie Performance:
Analog zur Reliability werden für die maximal erwarteten Nutzungsdaten Antwortzeiten des Systems erfasst und als Metrik festgeschrieben.

* S wie Supportablity
Hier kann z.B. festgeschrieben werden, wie lange ein Durchgang durch die CI/CD-Pipeline (also von der IDE bis zum getesteten Code in der Produktivumgebung) dauern darf. Die Update-Zyklendauer, die verpflichtende Benutzung von Clean-Code Metriken (zyklomatische Komplexität, Testabdeckung) geben darüber hinaus ein Maß der Wartbarkeit, dass vereinbart werden kann.


### Gliederung von Anforderungen

Die Gliederung von Anforderungen kann aus unterschiedlichen Gründen sinnvoll sein:

* Im Vorfeld, um ggf. fehlende Anforderungen in Kategorien zu erkennen.

* In der Bearbeitung, um Ansatzpunkte für eine Priorisierung zu erhalten oder eine Reihenfolge festzulegen.


Es gibt eine Vielzahl an möglichen Herangehensweisen

Gliederung nach Systemkomponenten:

* Aufteilung nach Abstraktionsebenen (geringer Abstraktionsgrad: "Nah am User" / hoher Abstraktionsgrad: "Nah an den Bits und Bytes")

* Aufteilung nach dem Schichtenmodell: Persistenzschicht (Datenhaltung), Verabeitungsschicht (Logik) und Präsentationsschicht (UserInterface)

Gliederung nach Realisierungspriorität:

* Aufteilung in optionale oder zwingende Anforderungen (wird oft im Pflichtenheft genutzt)

* Erweiterung durch **MoSCoW-Methode**:

|Must have|Should have| Can have|Won’t have|
|--|--|--|--|
|Erste Priorität: nicht verhandelbar, Fehlen würde zu Projetscheitern führen. MUST = Minimal usable Subset|Zweite Priorität: hohe Relevanz, sollten wenn immer möglich realisiert werden|"Nice to have": Werden umgesetzt, wenn noch Kapazitäten vorhanden sind.|Zeitlich nicht kritische Anforderungen, die aber technisch sinnvoll / wichtig sind. Bilden die Grundlage für nächste Produktiterationen|

* Mit dem **KANO-Modell** werden Anforderungen per Umfrage gewertet und in Kategorien eingeteilt:

|Basis-Merkmale|Leistung-Merkmale|Begeisterungs-Merkmale|Unerhebliche Merkmale|Rückweisungs-Merkmale|
|--|--|--|--|--|
|so grundlegend, dass nur ihr Fehlen bemerkt wird, nicht jedoch, dass sie umgesetzt wurden.|Der Grad ihrer Umsetzung schafft Zufriedenheit, ihre Umsetzung wird erwartet|Überraschende Funktionen, die nicht erwartet wurden und die von der Konkurrenz abheben|Umsetzung führt nicht zu Zufriedenheit, fehlen auch nicht zu Unzufriedenheit|Realisierung führt zu Unzufriedenheit - |

* Ordnung nach dem innewohnendem Risiko: Anforderungen mit dem höchsten Projektrisiko (deren Scheitern den Projekterfolg verhindern kann) sollten möglichst früh umgesetzt werden.

Zusammenfassung von Anforderungen zu Gruppen:

* Gruppierung als **Minimal Viable Product** (M.V.P.), dass die kleinste Einheit darstellt, die bereits einen Nutzen für den Anwender erzeugt (oft Ausgangspunkt bei iterativem Vorgehen)
* Aufteilung als **Prototyp**
  * vertikaler Prototyp: enthält bereits in der ersten Iteration einen "Durchstich" durch alle Softwareschichten
  * horizontaler Prototyp: Realisierung einer Schicht / einer Teilkomponente, die nicht durch alle Schichten führt
* Aufteilung in Milestones (Gruppierung z.B. um Rechnungsstellung innerhalb der Bearbeitung zu ermöglichen)

## Phase 3: Prüfen und Bewerten der Anforderungen (_requirements validation_)

Bereits vor der Durchführung sollte sichergestellt werden, dass die dokumentierten Anforderungen von allen _Stakeholder_ gleich verstanden werden und deren Bedürfnisse erfüllen. Das Risiko des Scheiterns von Projektbestandteilen kann so minimiert werden.

* Wurden Konflikte zwischen konkurrierenden Anforderungen / _Stakeholdern_ gelöst?

* Entspricht die Priorisierung der Anforderungen dem Wunsch des Kunden oder ist mit diesem einvernehmlich abgesprochen?

* Wurden die Wünsche und Bedürfnisse der _Stakeholder_ durch die dokumentierten Anforderungen hinreichend abgedeckt?

* Wurde ein gemeinsames Verständnis der Anforderung bei allen Projektbeteiligten erreicht?

* Gibt es einvernehmen über die Systemgrenzen und über die Randbedingungen?

## Phase 4: Verwalten der Anforderungen (_requirements management_)

Im Projektverlauf werden sich die Anforderungen an das System ändern. Hieraus können sich eine ganze Reihe von Konflikten und Problemen ergeben, die durch ein gutes Anforderungsmanagement leichter gelöst werden können. Eine gute Dokumentation, eine regelmäßige und gute Kommunikation im Team und mit dem Kunden sind hierbei von Vorteil.

## Weitere Literatur zu Requirement-Engineering

- Unterlagen des International Requirement Engineering Boards zur Zertifizierung als "Certified Professional for Requirements Engineering Foundation Level": [https://www.ireb.org/de/cpre/foundation/](https://www.ireb.org/de/cpre/foundation/)

## _Quellen und offene Ressourcen (OER)_

Die Ursprungstexte (als Markdown), Grafiken und zugrunde liegende Diagrammquelltexte finden sich (soweit möglich in weiterbearbeitbarer Form) in folgendem git-Repository:

[https://gitlab.com/oer-informatik/uml/umlanwendungsfall](https://gitlab.com/oer-informatik/uml/umlanwendungsfall).

Sie sind bei Namensnennung (Hannes Stein) zur Nutzung als Open Education Resource (OER) freigegeben gemäß der [Creative Commons Namensnennung 4.0 International Lizenz (CC BY 4.0)](https://creativecommons.org/licenses/by/4.0/deed.de).

[![Creative Commons Lizenzvertrag](https://i.creativecommons.org/l/by/4.0/88x31.png)](https://creativecommons.org/licenses/by/4.0/deed.de)
